#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.section .rodata

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

NewLineCharacter:
	.ascii "\n"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.text

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	.global Copy

Copy:
	xorq %rax, %rax
Copy_Start:
	cmp %rax, %rdx
	je Copy_End
	movb (%rdi, %rax, 1), %cl
	movb %cl, (%rsi, %rax, 1)
	incq %rax
	jmp Copy_Start
Copy_End:
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Move

Move:
	push %r12
	push %r13
	push %r14
	movq %rdi, %r12
	movq %rsi, %r13
	movq %rdx, %r14
	subq %r14, %rsp
	lea (%rsp), %rsi
	call Copy
	lea (%rsp), %rdi
	movq %r13, %rsi
	movq %r14, %rdx
	call Copy
	addq %r14, %rsp
	pop %r14
	pop %r13
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Set

Set:
	xorq %rax, %rax
Set_Start:
	cmp %rsi, %rax
	je Set_End
	movb %dl, (%rdi, %rax, 1)
	jmp Set_Start
Set_End:
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global SetZero

SetZero:
	xorb %dl, %dl
	call Set
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringLength

StringLength:
	xorq %rax, %rax
StringLength_Start:
	movb (%rdi, %rax, 1), %cl
	test %cl, %cl
	jz StringLength_End
	incq %rax
	jmp StringLength_Start
StringLength_End:
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Print

Print:
	movq %rsi, %rdx
	movq %rdi, %rsi
	movq $0x01, %rdi
	movq $0x01, %rax
	syscall
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintZ

PrintZ:
	push %r12
	movq %rdi, %r12
	call StringLength
	movq %rax, %rsi
	movq %r12, %rdi
	call Print
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global NewLine

NewLine:
	lea NewLineCharacter, %rdi
	movq $0x01, %rsi
	call Print
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintLine

PrintLine:
	call Print
	call NewLine
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintLineZ

PrintLineZ:
	call PrintZ
	call NewLine
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global IntegerToCharacter

IntegerToCharacter:
	cmp $0x09, %rdi
	jg IntegerToCharacter_Letter
	jmp IntegerToCharacter_Number
IntegerToCharacter_Number:
	addq $0x30, %rdi
	movq %rdi, %rax
	ret
IntegerToCharacter_Letter:
	addq $0x41, %rdi
	movq %rdi, %rax
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringReverse

StringReverse:
	push %r12
	movq %rdi, %r12
	call StringLength
	xorq %rcx, %rcx
	movq %rax, %rdx
	decq %rdx
StringReverse_Start:
	cmp %rcx, %rdx
	jle StringReverse_End
	movb (%r12, %rcx, 1), %r8b
	movb (%r12, %rdx, 1), %r9b
	movb %r8b, (%r12, %rdx, 1)
	movb %r9b, (%r12, %rcx, 1)
	incq %rcx
	decq %rdx
	jmp StringReverse_Start
StringReverse_End:
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global IntegerToString

IntegerToString:
	test %rdi, %rdi
	jnz IntegerToString_NonZero
	movb $0x30, %al
	movb %al, (%rsi)
	xorb %al, %al
	movb %al, 1(%rsi)
	ret
IntegerToString_NonZero:
	push %r12
	push %r13
	push %r14
	push %r15
	movq %rdi, %r12
	movq %rsi, %r13
	movq %rdx, %r14
	xorq %r15, %r15
IntegerToString_Begin:
	test %r12, %r12
	jz IntegerToString_End
	movq %r12, %rax
	xorq %rdx, %rdx
	divq %r14
	movq %rax, %r12
	movq %rdx, %rdi
	call IntegerToCharacter
	movb %al, (%r13, %r15, 1)
	incq %r15
	jmp IntegerToString_Begin
IntegerToString_End:
	xorb %al, %al
	movb %al, (%r13, %r15, 1)
	movq %r13, %rdi
	call StringReverse
	pop %r15
	pop %r14
	pop %r13
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintInteger

PrintInteger:
	subq $0x50, %rsp
	movq %rsi, %rdx
	lea (%rsp), %rsi
	call IntegerToString
	lea (%rsp), %rdi
	call PrintZ
	addq $0x50, %rsp
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintLineInteger

PrintLineInteger:
	call PrintInteger
	call NewLine
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global SignedIntegerToString

SignedIntegerToString:
	movq $0x8000000000000000, %rcx
	test %rcx, %rdi
	jnz SignedIntegerToString_Negative
	movq $0x0A, %rdx
	call IntegerToString
	ret
SignedIntegerToString_Negative:
	push %r12
	push %r13
	movq %rdi, %r12
	movq %rsi, %r13
	negq %rdi
	movq $0x0A, %rdx
	call IntegerToString
	movq %r13, %rdi
	call StringLength
	movq %rax, %rdx
	incq %rdx
	movq %r13, %rdi
	movq %r13, %rsi
	incq %rsi
	call Move
	movb $0x2D, %al
	movb %al, (%r13)
	pop %r13
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintSignedInteger

PrintSignedInteger:
	subq $0x50, %rsp
	lea (%rsp), %rsi
	call SignedIntegerToString
	lea (%rsp), %rdi
	call PrintZ
	addq $0x50, %rsp
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global PrintLineSignedInteger

PrintLineSignedInteger:
	call PrintSignedInteger
	call NewLine
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Exit

Exit:
	movq $0x3C, %rax
	syscall

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global ExitZero

ExitZero:
	xorq %rdi, %rdi
	call Exit

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Compare

Compare:
	xorq %rax, %rax
Compare_Start:
	cmp %rax, %rdx
	je Compare_Success
	movb (%rdi, %rax, 1), %r8b
	movb (%rsi, %rax, 1), %r9b
	cmp %r8b, %r9b
	jne Compare_Fail
	incq %rax
	jmp Compare_Start
Compare_Success:
	movq $0x01, %rax
	ret
Compare_Fail:
	xorq %rax, %rax
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringCompare

StringCompare:
	push %r12
	push %r13
	push %r14
	movq %rdi, %r12
	movq %rsi, %r13
	call StringLength
	movq %rax, %r14
	movq %r13, %rdi
	call StringLength
	cmp %r14, %rax
	jne StringCompare_Fail
	movq %r12, %rdi
	movq %r13, %rsi
	movq %r14, %rdx
	call Compare
	pop %r14
	pop %r13
	pop %r12
	ret
StringCompare_Fail:
	xorq %rax, %rax
	pop %r14
	pop %r13
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringCopy

StringCopy:
	push %r12
	push %r13
	movq %rdi, %r12
	movq %rsi, %r13
	call StringLength
	movq %r12, %rdi
	movq %r13, %rsi
	movq %rax, %rdx
	incq %rdx
	call Copy
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Lowercase

Lowercase:
	cmp $0x5A, %dil
	jg Lowercase_Skip
	cmp $0x41, %dil
	jl Lowercase_Skip
	movb %dil, %al
	addb $0x20, %al
	ret
Lowercase_Skip:
	movb %dil, %al
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global Uppercase

Uppercase:
	cmp $0x61, %dil
	jl Uppercase_Skip
	cmp $0x7A, %dil
	jg Uppercase_Skip
	movb %dil, %al
	subb $0x20, %al
	ret
Uppercase_Skip:
	movb %dil, %al
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringTransform

StringTransform:
	push %r12
	push %r13
	push %r14
	push %r15
	movq %rdi, %r12
	movq %rsi, %r13
	call StringLength
	movq %rax, %r14
	xorq %r15, %r15
StringTransform_Start:
	cmp %r15, %r14
	je StringTransform_End
	movb (%r12, %r15, 1), %dil
	call *%r13
	movb %al, (%r12, %r15, 1)
	incq %r15
	jmp StringTransform_Start
StringTransform_End:
	pop %r15
	pop %r14
	pop %r13
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringLowercase

StringLowercase:
	lea Lowercase, %rsi
	call StringTransform
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringUppercase

StringUppercase:
	lea Uppercase, %rsi
	call StringTransform
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringCompareI

StringCompareI:
	push %rbx
	push %r12
	push %r13
	push %r14
	push %r15
	movq %rdi, %r12
	movq %rsi, %r13
	call StringLength
	movq %rax, %r14
	movq %r13, %rdi
	call StringLength
	cmp %r14, %rax
	jne StringCompare_Fail
	xorq %rbx, %rbx
StringCompareI_Start:
	cmp %rbx, %r14
	je StringCompareI_End
	movb (%r12, %rbx, 1), %dil
	call Lowercase
	movb %al, %r15b
	movb (%r13, %rbx, 1), %dil
	call Lowercase
	cmp %al, %r15b
	jne StringCompareI_Fail
	incq %rbx
	jmp StringCompareI_Start
StringCompareI_End:
	movq $1, %rax
	pop %r15
	pop %r14
	pop %r13
	pop %r12
	pop %rbx
	ret
StringCompareI_Fail:
	xorq %rax, %rax
	pop %r15
	pop %r14
	pop %r13
	pop %r12
	pop %rbx
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global CharacterToInteger

CharacterToInteger:
	xorq %rax, %rax
	cmp $0x39, %dil
	jle CharacterToInteger_Number
	jmp CharacterToInteger_Letter
CharacterToInteger_Number:
	subb $0x30, %dil
	movb %dil, %al
	ret
CharacterToInteger_Letter:
	subb $0x41, %dil
	addb $0x0A, %dil
	movb %dil, %al
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringToInteger

StringToInteger:
	push %rbx
	push %r12
	push %r13
	push %r14
	push %r15
	movq %rdi, %r12
	movq %rsi, %r13
	xorq %r15, %r15
	call StringLength
	test %rax, %rax
	jz StringToInteger_End
	decq %rax
	movq %rax, %r14
	movq $0x01, %rbx
StringToInteger_Start:
	movb (%r12, %r14, 1), %dil
	call CharacterToInteger
	mulq %rbx
	addq %rax, %r15
	test %r14, %r14
	jz StringToInteger_End
	decq %r14
	movq %rbx, %rax
	mulq %r13
	movq %rax, %rbx
	jmp StringToInteger_Start
StringToInteger_End:
	movq %r15, %rax
	pop %r15
	pop %r14
	pop %r13
	pop %r12
	pop %rbx
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	.global StringToSignedInteger

StringToSignedInteger:
	push %r12
	push %r13
	movq %rdi, %r12
	movb (%r12), %cl
	xorq %r13, %r13
	cmp $0x2B, %cl
	jne StringToSignedInteger_NoPositive
	incq %r12
StringToSignedInteger_NoPositive:
	cmp $0x2D, %cl
	jne StringToSignedInteger_NoNegative
	incq %r12
	movq $0x01, %r13
StringToSignedInteger_NoNegative:
	movq %r12, %rdi
	movq $0x0A, %rsi
	call StringToInteger
	test %r13, %r13
	jz StringToSignedInteger_Skip
	negq %rax
StringToSignedInteger_Skip:
	pop %r13
	pop %r12
	ret

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

